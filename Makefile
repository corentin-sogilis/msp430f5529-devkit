# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS
#
# Makefile : Build Docker image for MSP430F5529 development environment
# Author : Corentin <corentin@sogilis.com>, Alex <alexandre@sogilis.com>
#
# SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS SOGILIS

# Required files at root directory to build the docker image :
# - msp430-gcc-5.3.0.224_linux64.tar.bz2
# - msp430-gcc-support-files-1.194.zip
# - msp430.dll_developer_package_rev_3.09.001.002.zip
# - mspdebug (binary from mspdebug compilation, version >= 0.24 )

IMAGE_NAME = sogicoco/msp430f5529-devkit

all : main

main : Dockerfile
	docker build -t $(IMAGE_NAME) .

run :
	docker run --rm --privileged -v $(shell pwd):/opt/workspace -it $(IMAGE_NAME) /bin/bash

clean :
	docker rmi $(IMAGE_NAME)

################################################################################
# DEPENDENCIES
################################################################################

vendor:
	mkdir -p vendor

vendor/msp430-gcc-5.3.0.224_linux64.tar.bz2: vendor
	wget -P vendor http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-5.3.0.224_linux64.tar.bz2

vendor/msp430-gcc-support-files-1.194.zip: vendor
	wget -P vendor http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-support-files-1.194.zip

vendor/msp430.dll_developer_package_rev_3.09.001.002.zip: vendor
	wget -P vendor http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPDS/latest/exports/msp430.dll_developer_package_rev_3.09.001.002.zip

vendor/mspdebug/Makefile: vendor
	git clone https://github.com/dlbeer/mspdebug vendor/mspdebug

vendor/mspdebug/mspdebug:vendor/mspdebug/Makefile vendor
	cd vendor/mspdebug && make

fetch_dependencies : vendor/msp430-gcc-5.3.0.224_linux64.tar.bz2 vendor/msp430-gcc-support-files-1.194.zip vendor/msp430.dll_developer_package_rev_3.09.001.002.zip vendor/mspdebug/mspdebug
